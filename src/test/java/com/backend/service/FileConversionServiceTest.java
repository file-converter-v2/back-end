package com.backend.service;

import com.backend.model.FileConversion;
import com.backend.repository.FileConversionRepository;
import com.dropbox.core.DbxException;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class FileConversionServiceTest {
    @Mock
    private FileConversionRepository repository;

    @InjectMocks
    private FileConversionService service;

    @InjectMocks
    private DropboxService dropboxService;

    private final String testFile = System.getProperty("user.dir") + "/src/test/java/com/backend/service/data/ki0Z456.jpg";
    private final String temporaryLink = "https://content.dropboxapi.com/apitl/1/";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        List<FileConversion> conversions = new ArrayList<>();
        FileConversion entry = new FileConversion(
            UUID.fromString("57372138-0444-46f1-b895-7008965dc109"),
            UUID.fromString("e8f46314-6bb1-4343-8772-018f4e8226c3"),
            "1438312613278",
            "JPG",
            "PNG",
            new Date());

        conversions.add(entry);
        when(service.findAll()).thenReturn(conversions);
        when(repository.findAll()).thenReturn(conversions);
    }

    @Test
    public void testGetFileConversions() {
        List<FileConversion> results = service.findAll();

        for (FileConversion result : results) {
            System.out.println(result.toString());
        }

        assertTrue(results.size() >= 0);
    }

    // TODO:
    // Write test for create
    // Write test for download
    // Write test for delete

//    @Test
//    public void testCreateFileConversion() throws IOException, DbxException {
//        File file = new File(testFile);
//        FileInputStream is = new FileInputStream(file);
//        MultipartFile multipartFile = new MockMultipartFile(file.getName(), file.getName(), MediaType.IMAGE_JPEG.toString(), is);
//
//        when(service.create(multipartFile, "BMP")).thenReturn(temporaryLink);
//        String result = service.create(multipartFile, "BMP");
//
//        assertTrue(result.contains("https://content.dropboxapi.com"));
//    }

//    @Test
//    public void testDownloadFileConversion() throws DbxException {
//        List<FileConversion> results = service.findAll();
//        int lastIdx = results.size() - 1;
//
//        if (lastIdx >= 0) {
//            UUID latestConversionUUID = results.get(lastIdx).getFileId();
//
//            when(service.download(latestConversionUUID)).thenReturn(temporaryLink);
//            when(dropboxService.getTemporaryLink("/home/ivan/e8f46314-6bb1-4343-8772-018f4e8226c3/1438312613278.png")).thenReturn(temporaryLink);
//
//            String result = service.download(latestConversionUUID);
//            assertTrue(result.contains("https://content.dropboxapi.com"));
//        }
//    }

//    @Test
//    public void testDeleteFileConversion() {
//        List<FileConversion> results = service.findAll();
//        int lastIdx = results.size() - 1;
//
//        if (lastIdx >= 0) {
//            UUID latestConversionUUID = results.get(lastIdx).getFileId();
//            service.delete(latestConversionUUID);
//        }
//    }
}
