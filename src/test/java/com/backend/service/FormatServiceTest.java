package com.backend.service;

import com.backend.model.Format;
import com.backend.repository.FormatRepository;
import com.backend.service.FormatService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class FormatServiceTest {
    @Mock
    private FormatRepository repository;

    @InjectMocks
    private FormatService service;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetFormats() {
        List<Format> formats = new ArrayList<>();

        formats.add(new Format(UUID.fromString("c7fe122-3e3b-11ea-8d2b-bb63141ca3f2"), "PNG",  "image/png"));
        formats.add(new Format(UUID.fromString("6c7ff9b4-3e3b-11ea-8d2b-937e81e123fb"), "BMP",  "image/bmp"));
        formats.add(new Format(UUID.fromString("6c80090e-3e3b-11ea-8d2b-cf6c5a1aa296"), "JPG",  "image/jpeg"));

        when(repository.findAll()).thenReturn(formats);
        List<Format> results = service.findAll();

        assertEquals(formats.size(), results.size());
    }
}
