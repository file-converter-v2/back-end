package com.backend.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class FileSystemServiceTest {
    private final String testDirectory = System.getProperty("user.home") + "/test-directory";
    private final String newFile = testDirectory + "/test-data.txt";
    private final String testFile = System.getProperty("user.dir") + "/src/test/java/com/backend/service/data/test-data.txt";

    private Path testDirectoryPath;

    @InjectMocks
    private FileSystemService service;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        testDirectoryPath = Paths.get(testDirectory);
    }

    @After
    public void tearDown() throws IOException {
        Path newFilePath = Paths.get(newFile);
        Files.deleteIfExists(newFilePath);
        Files.deleteIfExists(testDirectoryPath);
    }

    @Test
    public void testCreateDirectory() throws IOException {
        service.createDirectory(testDirectory);
        assertTrue(Files.exists(testDirectoryPath));
    }

    @Test
    public void testCreateFile() throws IOException {
        InputStream is = new FileInputStream(testFile);

        if (!Files.exists(testDirectoryPath)) {
            service.createDirectory(testDirectory);
        }
        service.createFile(is, newFile);

        assertTrue(Files.exists(Paths.get(newFile)));
    }

    @Test
    public void testDelete() throws IOException {
        service.delete(newFile);
        assertTrue(!Files.exists(Paths.get(newFile)));
    }
}
