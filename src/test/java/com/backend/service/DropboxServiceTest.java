package com.backend.service;

import com.dropbox.core.DbxException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class DropboxServiceTest {
    private final String testFile = System.getProperty("user.dir") + "/src/test/java/com/backend/service/data/test-data.txt";

    @InjectMocks
    private DropboxService service;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testUploadAndDownload() throws IOException, DbxException {
        service.upload(testFile);
        String tempLink = service.getTemporaryLink(testFile);

        assertTrue(tempLink.length() > 0);
    }
}
