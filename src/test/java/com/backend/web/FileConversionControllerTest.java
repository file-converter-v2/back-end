package com.backend.web;

import com.backend.model.FileConversion;
import com.backend.service.FileConversionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class FileConversionControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private FileConversionService service;

    private final String testFile = System.getProperty("user.dir") + "/src/test/java/com/backend/web/data/ki0Z456.jpg";

    @Test
    public void testGetFileConversions() throws Exception {
        List<FileConversion> conversions = service.findAll();
        this.mockMvc.perform(
            get("/conversions")
            .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(conversions.size())))
        .andDo(print());
    }

    @Test
    public void testCreateFileConversion() throws Exception {
        Path path = Paths.get(testFile);
        String contentType = "image/jpeg";
        byte[] content = Files.readAllBytes(path);
        MockMultipartFile file = new MockMultipartFile("file", "ki0Z456.jpg", contentType, content);

        this.mockMvc.perform(
            multipart("/conversions")
            .file(file)
            .param("format", "BMP")
        )
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("https://content.dropboxapi.com")))
        .andDo(print());
    }

    @Test
    public void testDownloadFileConversion() throws Exception {
        List<FileConversion> conversions = service.findAll();
        UUID latestConversionUUID = conversions.get(conversions.size() - 1).getFileId();

        if (latestConversionUUID.toString().length() > 0) {
            this.mockMvc.perform(
                get("/conversions/download")
                .header("uuid", latestConversionUUID)
            )
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("https://content.dropboxapi.com")))
            .andDo(print());
        }
    }

    @Test
    public void testDeleteFileConversion() throws Exception {
        List<FileConversion> conversions = service.findAll();
        UUID latestConversionUUID = conversions.get(conversions.size() - 1).getFileId();

        if (latestConversionUUID.toString().length() > 0) {
            this.mockMvc.perform(
                delete("/conversions")
                .header("uuid", latestConversionUUID)
            )
            .andExpect(status().isOk())
            .andDo(print());
        }
    }
}
