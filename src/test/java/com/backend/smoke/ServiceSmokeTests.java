package com.backend.smoke;

import com.backend.service.DropboxService;
import com.backend.service.FileConversionService;
import com.backend.service.FileSystemService;
import com.backend.service.FormatService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ServiceSmokeTests {
    @Autowired
    private DropboxService dropboxService;

    @Autowired
    private FileConversionService fileConversionService;

    @Autowired
    private FileSystemService fileSystemService;

    @Autowired
    private FormatService formatService;

    @Test
    public void contextLoads() {
        assertThat(dropboxService).isNotNull();
        assertThat(fileConversionService).isNotNull();
        assertThat(fileSystemService).isNotNull();
        assertThat(formatService).isNotNull();
    }
}
