package com.backend.smoke;

import com.backend.controller.FileConversionController;
import com.backend.controller.FormatController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ControllerSmokeTests {
    @Autowired
    private FileConversionController fileConversionController;

    @Autowired
    private FormatController formatController;

    @Test
    public void contextLoads() {
        assertThat(fileConversionController).isNotNull();
        assertThat(formatController).isNotNull();
    }
}
