package com.backend.smoke;

import com.backend.model.FileConversion;
import com.backend.model.Format;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ModelSmokeTests {
    @MockBean
    private FileConversion fileConversion;

    @MockBean
    private Format format;

    @Test
    public void contextLoads() {
        assertThat(fileConversion).isNotNull();
        assertThat(format).isNotNull();
    }
}
