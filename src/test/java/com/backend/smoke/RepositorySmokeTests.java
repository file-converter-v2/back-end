package com.backend.smoke;

import com.backend.repository.FileConversionRepository;
import com.backend.repository.FormatRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class RepositorySmokeTests {
    @Autowired
    private FileConversionRepository fileConversionRepository;

    @Autowired
    private FormatRepository formatRepository;

    @Test
    public void contextLoads() {
        assertThat(fileConversionRepository).isNotNull();
        assertThat(formatRepository).isNotNull();
    }
}
