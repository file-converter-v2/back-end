CREATE OR REPLACE FUNCTION insert_formats() RETURNS void LANGUAGE plpgsql AS '
BEGIN
    IF NOT EXISTS (SELECT id FROM formats
                   WHERE id = ''6c7fe122-3e3b-11ea-8d2b-bb63141ca3f2''
                   OR    id = ''6c7ff9b4-3e3b-11ea-8d2b-937e81e123fb''
                   OR    id = ''6c80090e-3e3b-11ea-8d2b-cf6c5a1aa296''
    ) THEN
        INSERT INTO formats(id, name, type) VALUES
        (''6c7fe122-3e3b-11ea-8d2b-bb63141ca3f2'', ''PNG'',  ''image/png''),
        (''6c7ff9b4-3e3b-11ea-8d2b-937e81e123fb'', ''BMP'',  ''image/bmp''),
        (''6c80090e-3e3b-11ea-8d2b-cf6c5a1aa296'', ''JPG'',  ''image/jpeg'');
    END IF;
END
';

SELECT insert_formats();
