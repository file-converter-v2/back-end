package com.backend;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class BackEndApplication {
	public static void main(String[] args) {
		new SpringApplicationBuilder(BackEndApplication.class).run(args);
	}
}
