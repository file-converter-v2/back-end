package com.backend.service;

import com.backend.model.Format;

import java.util.List;

public interface IFormatService {
    List<Format> findAll();
}
