package com.backend.service;

import com.dropbox.core.DbxException;

import java.io.IOException;

public interface IDropboxService {
    void upload(String src) throws IOException, DbxException;
    String getTemporaryLink(String src) throws DbxException;
}
