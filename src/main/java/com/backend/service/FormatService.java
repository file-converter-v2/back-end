package com.backend.service;

import com.backend.model.Format;
import com.backend.repository.FormatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("formatService")
public class FormatService implements IFormatService {
    @Autowired
    private FormatRepository repository;

    public List<Format> findAll() {
        return repository.findAll();
    }
}
