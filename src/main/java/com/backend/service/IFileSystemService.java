package com.backend.service;

import java.io.IOException;
import java.io.InputStream;

public interface IFileSystemService {
    void createDirectory(String dest) throws IOException;
    void delete(String dest) throws IOException;
    void createFile(InputStream fileStream, String dest) throws IOException;
}
