package com.backend.service;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Service("dropboxService")
public class DropboxService implements IDropboxService {
    private final DbxClientV2 client;

    public DropboxService() {
        DbxRequestConfig config = new DbxRequestConfig("dropbox/java-tutorial", "en_US");
        String ACCESS_TOKEN = "jzvEwa3l4y0AAAAAAAAAAZlCNeVrn4R9apX_wXTnvdyHNTBt_jnVdUZwggUnFuJZ";
        this.client = new DbxClientV2(config, ACCESS_TOKEN);
    }

    public void upload(String src) throws IOException, DbxException {
        InputStream in = new FileInputStream(src);
        client.files().upload(src).uploadAndFinish(in);
    }

    public String getTemporaryLink(String src) throws DbxException {
        return client.files().getTemporaryLink(src).getLink();
    }
}
