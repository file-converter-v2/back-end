package com.backend.service;

import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service("fileSystemService")
public class FileSystemService implements IFileSystemService {
    public void createDirectory(String dest) throws IOException {
        Path path = Paths.get(dest);
        Files.createDirectory(path);
    }

    public void delete(String dest) throws IOException {
        Path path = Paths.get(dest);
        Files.deleteIfExists(path);
    }

    public void createFile(InputStream fileStream, String dest) throws IOException {
        try (OutputStream os = new FileOutputStream(dest)) {
            byte[] buf = new byte[1024];

            int bytesRead;
            while ((bytesRead = fileStream.read(buf)) > 0) {
                os.write(buf, 0, bytesRead);
            }
        }
    }
}
