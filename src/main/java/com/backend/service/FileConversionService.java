package com.backend.service;

import com.backend.model.FileConversion;
import com.backend.repository.FileConversionRepository;
import com.dropbox.core.DbxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service("fileConversionService")
public class FileConversionService implements IFileConversionService {
    private final String homeDirectory = System.getProperty("user.home");

    @Autowired
    private FileConversionRepository repository;

    @Autowired
    private DropboxService dropboxService;

    @Autowired
    private FileSystemService fileSystemService;

    public void delete(UUID id) {
        FileConversion conversion = getFileConversionByFileId(id);
        repository.deleteById(conversion.getId());
    }

    public List<FileConversion> findAll() {
        return repository.findAll();
    }

    public String download(UUID id) throws DbxException {
        FileConversion conversion = getFileConversionByFileId(id);
        String dir = homeDirectory + "/" + conversion.getFileId().toString();
        String fileName = conversion.getFileName() + "." + conversion.getNewFormat().toLowerCase();
        return dropboxService.getTemporaryLink(dir + "/" + fileName);
    }

    public String create(MultipartFile file, String format) throws IOException, DbxException {
        UUID id = UUID.randomUUID();
        FileConversion conversion = new FileConversion();
        String[] fileNameContents = file.getOriginalFilename().split("\\.");
        String fileDirectory = homeDirectory + "/" + id.toString();

        conversion.setFileId(id);
        conversion.setFileName(fileNameContents[0]);
        conversion.setOldFormat(fileNameContents[1].toUpperCase());
        conversion.setNewFormat(format);
        conversion.setCreatedAt(Date.from(Instant.now()));

        String originalFilePath = fileDirectory + "/" + conversion.getFileName() + "." + conversion.getOldFormat().toLowerCase();
        String convertedFilePath = fileDirectory + "/" + conversion.getFileName() + "." + conversion.getNewFormat().toLowerCase();

        fileSystemService.createDirectory(fileDirectory);
        fileSystemService.createFile(file.getInputStream(), originalFilePath);

        convertFile(conversion);
        dropboxService.upload(convertedFilePath);
        repository.save(conversion);

        fileSystemService.delete(originalFilePath);
        fileSystemService.delete(convertedFilePath);
        fileSystemService.delete(fileDirectory);
        return dropboxService.getTemporaryLink(convertedFilePath);
    }

    private void convertFile(FileConversion conversion) throws IOException {
        String dir = homeDirectory + "/" + conversion.getFileId().toString();
        String oldFileName = conversion.getFileName() + "." + conversion.getOldFormat().toLowerCase();
        String newFileName = conversion.getFileName() + "." + conversion.getNewFormat().toLowerCase();

        File input = new File(dir + "/" + oldFileName);
        BufferedImage image = ImageIO.read(input);
        File output = new File(dir + "/" + newFileName);

        ImageIO.write(image, conversion.getNewFormat().toLowerCase(), output);
    }

    private FileConversion getFileConversionByFileId(UUID fileId) {
        List<FileConversion> conversions = repository.findAll();
        FileConversion selectedConversion = new FileConversion();

        for (FileConversion conversion : conversions) {
            if (conversion.getFileId().equals(fileId)) {
                selectedConversion = conversion;
            }
        }

        return selectedConversion;
    }
}
