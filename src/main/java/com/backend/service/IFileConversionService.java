package com.backend.service;

import com.backend.model.FileConversion;
import com.dropbox.core.DbxException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public interface IFileConversionService {
    void delete(UUID id);
    List<FileConversion> findAll();
    String download(UUID id) throws DbxException;
    String create(MultipartFile file, String format) throws IOException, DbxException;
}

