package com.backend.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "formats")
public class Format {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    public Format() { super(); }

    public Format(UUID id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public void setId(UUID id)       { this.id = id; }
    public void setName(String name) { this.name = name; }
    public void setType(String type) { this.type = type; }

    public UUID getId()     { return this.id; }
    public String getName() { return this.name; }
    public String getType() { return this.type; }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        hash = 79 * hash + Objects.hashCode(this.name);
        hash = 79 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Format other = (Format) obj;

        if (this == obj) { return true; }
        if (obj == null) { return false; }
        if (!Objects.equals(getClass(), obj.getClass())) { return false; }
        if (!Objects.equals(this.type, other.type)) { return false; }
        if (!Objects.equals(this.name, other.name)) { return false; }

        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Format {");
        sb.append("id=").append(id.toString());
        sb.append(", name=").append(name);
        sb.append(", type=").append(type);
        sb.append('}');
        return sb.toString();
    }
}
