package com.backend.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "conversions")
public class FileConversion {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "file_id")
    private UUID fileId;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "old_format")
    private String oldFormat;

    @Column(name = "new_format")
    private String newFormat;

    @Column(name = "created_at")
    private Date createdAt;

    public FileConversion() { super(); }

    public FileConversion(UUID id, UUID fileId, String fileName, String oldFormat, String newFormat, Date createdAt) {
        this.id = id;
        this.fileName = fileName;
        this.fileId = fileId;
        this.oldFormat = oldFormat;
        this.newFormat = newFormat;
        this.createdAt = createdAt;
    }

    public void setId(UUID id) { this.id = id; }
    public void setFileId(UUID fileId) { this.fileId = fileId; }
    public void setFileName(String fileName) { this.fileName = fileName; }
    public void setOldFormat(String oldFormat) { this.oldFormat = oldFormat; }
    public void setNewFormat(String newFormat) { this.newFormat = newFormat; }
    public void setCreatedAt(Date createdAt) { this.createdAt = createdAt; }

    public UUID getId() { return this.id; }
    public UUID getFileId() { return this.fileId; }
    public String getFileName() { return this.fileName; }
    public String getOldFormat() { return this.oldFormat; }
    public String getNewFormat() { return this.newFormat; }
    public Date getCreatedAt() { return this.createdAt; }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        hash = 79 * hash + Objects.hashCode(this.fileId);
        hash = 79 * hash + Objects.hashCode(this.fileName);
        hash = 79 * hash + Objects.hashCode(this.oldFormat);
        hash = 79 * hash + Objects.hashCode(this.newFormat);
        hash = 79 * hash + Objects.hashCode(this.createdAt);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final FileConversion other = (FileConversion) obj;

        if (this == obj) { return true; }
        if (obj == null) { return false; }
        if (getClass() != obj.getClass()) { return false; }
        if (!Objects.equals(this.createdAt, other.createdAt)) { return false; }
        if (!Objects.equals(this.newFormat, other.newFormat)) { return false; }
        if (!Objects.equals(this.oldFormat, other.oldFormat)) { return false; }
        if (!Objects.equals(this.fileName, other.fileName)) { return false; }
        if (!Objects.equals(this.fileId, other.fileId)) { return false; }

        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FileConversion {");
        sb.append("id=").append(id.toString());
        sb.append(", fileId=").append(fileId);
        sb.append(", fileName=").append(fileName);
        sb.append(", oldFormat=").append(oldFormat);
        sb.append(", newFormat=").append(newFormat);
        sb.append(", createdAt=").append(createdAt);
        sb.append('}');
        return sb.toString();
    }
}
