package com.backend.controller;

import com.backend.model.FileConversion;
import com.backend.service.FileConversionService;
import com.dropbox.core.DbxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/conversions")
public class FileConversionController {
    @Autowired
    private FileConversionService service;

    @GetMapping("")
    public List<FileConversion> findAll() {
        return service.findAll();
    }

    @GetMapping("/download")
    public String download(@RequestParam(name = "uuid", required = true) UUID id) throws DbxException {
        return service.download(id);
    }

    @PostMapping("")
    public String create(@RequestParam("file") MultipartFile file, @RequestParam("format") String format) throws IOException, DbxException {
        return service.create(file, format);
    }

    @DeleteMapping("")
    public void delete(@RequestParam(name = "uuid", required = true) UUID id) {
        service.delete(id);
    }
}
