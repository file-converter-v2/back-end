package com.backend.controller;

import com.backend.model.Format;
import com.backend.service.FormatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/formats")
public class FormatController {
    @Autowired
    private FormatService service;

    @GetMapping("")
    public List<Format> findAll() {
        return service.findAll();
    }

    @RequestMapping(method = RequestMethod.HEAD, name="")
    public void checkServer() {
        return;
    }
}
