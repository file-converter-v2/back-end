package com.backend.repository;

import com.backend.model.Format;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface FormatRepository extends JpaRepository<Format, UUID> {
}
