package com.backend.repository;

import com.backend.model.FileConversion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface FileConversionRepository extends JpaRepository<FileConversion, UUID> {
}
